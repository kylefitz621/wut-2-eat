const knex = require('./knex');

function getAllQuestions(){
    return knex("tblQuestions").select("*");
}

module.exports = getAllQuestions;