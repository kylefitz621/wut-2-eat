const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const {getAllQuestions} = require('./database/questions');

const HTTP_PORT =  process.env.port || 3001;

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

//Endpoint api for questions
app.get('/api', async (req, res)=> {
    const questions = await getAllQuestions();
    res.status(200).json({questions});
});

//listen on that port
app.listen(HTTP_PORT, () =>{
    console.log("Server is running on port " + HTTP_PORT)

});

//Default 404
app.use(function(req, res){
    res.status(404);
});

//Other API endpoints here