import React from "react";
import './App.css';
import { BrowserRouter as Router, Route, Routes} from "react-router-dom";
import About from "./components/pages/About";
import Home from "./components/pages/Home";
import Contact from "./components/pages/Contact";
import SignUp from "./components/pages/SignUp";
import QuizContainer from "./components/pages/QuizContainer";
import SignIn from "./components/pages/SignIn";

function App() {
  return (
    <Router>
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/signup" element={<SignUp />} />
            <Route path="/quiz" element={<QuizContainer />} />
            <Route path="/signin" element={<SignIn />} />
        </Routes>
    </Router>

  );
}

export default App;
