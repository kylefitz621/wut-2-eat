import React from "react";
import {Nav, NavLink, Bars, NavMenu, NavBtn, NavBtnLink} from "./NavbarElement";

const Navbar = () => {
    return (
        <Nav>

            <NavLink to={"/"}>
                <h1>Wut 2 Eat</h1>
            </NavLink>
            <Bars/>
            <NavMenu>
                <NavLink to="/">
                    Home
                </NavLink>
                <NavLink to="/quiz">
                    Quizzes
                </NavLink>
                <NavLink to="/about">
                    About
                </NavLink>
                <NavLink to="/contact">
                    Contact
                </NavLink>
            </NavMenu>
            <NavBtn>
                <NavBtnLink to="/signup">
                    Sign Up
                </NavBtnLink>
            </NavBtn>
        </Nav>

    )
}

export default Navbar;