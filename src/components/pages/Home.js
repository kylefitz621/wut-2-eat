import React from "react";
import Navbar from "../nav/Navbar";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";

const Home = () => {
    return (
        <>
            <Navbar/>
            <div className="App-header">
                <CssBaseline/>
                <Container>
                    <Box
                        sx={{
                            marginTop: 20,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <h1 className="">HOME PAGE</h1>

                    </Box>
                </Container>

            </div>
        </>
    )
}

export default Home;