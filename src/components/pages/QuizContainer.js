import React from 'react';
import Navbar from "../nav/Navbar";
// import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Button, CardActionArea, CardActions, colors, Container} from '@mui/material';
import CssBaseline from "@mui/material/CssBaseline";
import Card from "./Card";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
    gridContainer: {
        paddingLeft: "40px",
        paddingRight: "40px"
    }
});

export default function QuizContainer(props) {
    const classes = useStyles();
    return (
        <>
        <Navbar/>
    <div className="App-header">
        <CssBaseline/>
        {/*<Container>*/}
        {/*<Card sx={{ maxWidth: 300 }}>*/}
        {/*    <CardActionArea>*/}
        {/*        <CardMedia*/}
        {/*            component="img"*/}
        {/*            height="200"*/}
        {/*            image="salad_icon.png"*/}
        {/*            alt="What happened?"*/}
        {/*        />*/}

        {/*        <CardContent>*/}
        {/*            <Typography gutterBottom variant="h5" component="div">*/}
        {/*                Happy?*/}
        {/*            </Typography>*/}
        {/*            <Typography variant="body2" color="text.secondary">*/}
        {/*                Has this day been a great day!?!*/}
        {/*            </Typography>*/}
        {/*        </CardContent>*/}
        {/*    </CardActionArea>*/}
        {/*    <CardActions>*/}

        {/*        <Button variant='contained' size="small" color="primary" >*/}
        {/*            Yes?*/}
        {/*        </Button>*/}

        {/*    </CardActions>*/}
        {/*</Card>*/}
        {/*</Container>*/}

        {/*Add grid container to hold cards for questionnaire - will populate from back end*/}

        <Grid
            container
            spacing={4}
            className={classes.gridContainer}
            justifyContent={"center"}
        >
            <Grid item xs={12} sm={6} md={4}>
                <Card/>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Card />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Card />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Card />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <Card />
            </Grid>
        </Grid>
    </div>
        </>
    )
}

