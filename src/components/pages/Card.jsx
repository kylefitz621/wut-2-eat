import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    cardRoot: {
        minWidth: 200,
        maxWidth: 250,
        justifyContent: "space-evenly"
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 14,
        marginTop:14
    }
});

export default function OutlinedCard() {
    const classes = useStyles();

    return (
        <Card className={classes.cardRoot} variant="outlined">
            <CardContent>
                <Typography
                    className={classes.title}
                    color="textSecondary"
                    gutterBottom
                >
                    Question of the Day
                </Typography>
                <Typography variant="h5" component="h2">
                    How are you feeling?
                </Typography>
            </CardContent>
            <CardActions>
                <Button variant="contained" size="small" color="primary">I Feel That!</Button>
            </CardActions>
        </Card>
    );
}