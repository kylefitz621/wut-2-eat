import React from "react";
import Navbar from "../nav/Navbar";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Box from '@mui/material/Box';

const Contact = () => {
    return (
        <>
            <Navbar/>
            <div className="App-header">
                <CssBaseline/>
                <Container>
                    <Box
                        sx={{
                            marginTop: 20,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <h1 className="App">Contact information</h1>
                        <h5 className="App">Any suggestions, comments, or thoughts - please send to
                            suggestionbox@wut2eat.com</h5>

                    </Box>
                </Container>

            </div>
        </>
    )
}

export default Contact;