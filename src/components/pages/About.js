import React from "react";
import Navbar from "../nav/Navbar";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";

const About = () => {
    return (
        <>
            <Navbar/>
            <div className="App-header">
                <CssBaseline/>
                <Container>
                    <Box
                        sx={{
                            marginTop: 20,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <h5 className="App ">A fun and simple way for people like me, who are
                            indecisive when it comes to food at the end of a long day</h5>

                    </Box>
                </Container>

            </div>
        </>
    )
}

export default About;